# bcftools Singularity container
### Bionformatics package bcftools<br>
BCFtools is a set of utilities that manipulate variant calls in the Variant Call Format (VCF) and its binary counterpart BCF.<br>
bcftools Version: 1.10.2<br>
[https://github.com/samtools/bcftools]

Singularity container based on the recipe: Singularity.bcftools_v1.10.2

Package installation using Miniconda3 V4.7.12<br>

Image singularity (V>=3.3) is automaticly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

### build:
`sudo singularity build bcftools_v1.10.2.sif Singularity.bcftools_v1.10.2`

### image pull (singularity version >=3.3) with:<br>
`singularity pull bcftools_v1.10.2.sif oras://registry.forgemia.inra.fr/gafl/singularity/bcftools/bcftools:latest`


